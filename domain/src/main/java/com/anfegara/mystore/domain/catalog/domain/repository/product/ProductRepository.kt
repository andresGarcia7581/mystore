package com.anfegara.mystore.domain.catalog.domain.repository.product

import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.resource.Resource
import kotlinx.coroutines.flow.Flow

interface ProductRepository {

    suspend fun getProductCatalogByQuery(query: String): Resource<List<CatalogItem>>

    suspend fun getProductById(id: String): Resource<Product>

    suspend fun saveRecentProductQuery(query: String)

    fun getRecentProductQueries(): Flow<List<String>>

    fun getRecentProductQueriesByText(text: String): Flow<List<String>>
}