package com.anfegara.mystore.domain.billing.domain.model.payment

import java.lang.IllegalArgumentException

enum class CurrencyType(val id: String) {
    COLOMBIAN_PESO("COP"),
    ARGENTINE_PESO("ARS"),
    USD_DOLLAR("USD");

    companion object {
        fun getCurrencyTypeById(id: String) =
            values().firstOrNull { it.id == id }
                ?: throw IllegalArgumentException("$id is an invalid type")
    }
}