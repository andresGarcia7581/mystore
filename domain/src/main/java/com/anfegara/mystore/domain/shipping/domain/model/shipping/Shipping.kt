package com.anfegara.mystore.domain.shipping.domain.model.shipping

data class Shipping(
    val mode: String,
    val free: Boolean
)