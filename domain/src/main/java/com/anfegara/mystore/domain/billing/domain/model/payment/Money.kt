package com.anfegara.mystore.domain.billing.domain.model.payment

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Locale
import java.util.Currency

private const val MAXIMIUM_FRACTION_DIGITS = 2
private const val MINIMUM_FRACTION_DIGITS = 0

data class Money(
    val amount: BigDecimal,
    val currencyType: CurrencyType
) {

    fun toLocalizedString(locale: Locale = Locale.getDefault()): String {
        val moneyFormatter = NumberFormat.getCurrencyInstance(locale).apply {
            currency = Currency.getInstance(currencyType.id)
            maximumFractionDigits = MAXIMIUM_FRACTION_DIGITS
            minimumFractionDigits = MINIMUM_FRACTION_DIGITS
        }
        return moneyFormatter.format(amount)
    }
}