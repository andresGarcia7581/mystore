package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SaveRecentProductQueryUseCase @Inject constructor(private val productRepository: ProductRepository) {

    suspend operator fun invoke(query: String) {
        val queryFormatted = query.trim().toLowerCase(Locale.getDefault())
        withContext(Dispatchers.IO) { productRepository.saveRecentProductQuery(queryFormatted) }
    }
}