package com.anfegara.mystore.domain.catalog.domain.model.product

import com.anfegara.mystore.domain.billing.domain.model.payment.Money
import com.anfegara.mystore.domain.sales.domain.model.seller.Seller
import com.anfegara.mystore.domain.shipping.domain.model.shipping.Shipping

data class Product(
    val id: String,
    val name: String,
    val description: String,
    val images: List<String>,
    val price: Money,
    val soldQuantity: Int,
    val availableQuantity: Int,
    val warranty: String?,
    val attributes: List<ProductAttribute>,
    val shipping: Shipping,
    val seller: Seller
)