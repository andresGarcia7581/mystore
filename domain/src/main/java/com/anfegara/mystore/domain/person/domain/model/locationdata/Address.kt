package com.anfegara.mystore.domain.person.domain.model.locationdata

data class Address(
    val stateName: String,
    val cityName: String
)