package com.anfegara.mystore.domain.billing.domain.model.payment

import java.math.BigDecimal

data class Installments(
    val quantity: Int,
    val amount: Money,
    val rate: BigDecimal
)