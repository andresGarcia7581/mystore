package com.anfegara.mystore.domain.common.resource

import java.lang.Exception

sealed class ResourceError(override val message: String, override val cause: Throwable? = null) :
    Exception(message, cause) {

    class Network(override val message: String, override val cause: Throwable) :
        ResourceError(message, cause)

    class NotFound(override val message: String, override val cause: Throwable) :
        ResourceError(message, cause)

    class AccessDenied(override val message: String, override val cause: Throwable) :
        ResourceError(message, cause)

    class Unexpected(override val message: String, override val cause: Throwable) :
        ResourceError(message, cause)
}