package com.anfegara.mystore.domain.sales.domain.model.seller

import com.anfegara.mystore.domain.person.domain.model.locationdata.Address

data class Seller(
    val id: String,
    val address: Address
)