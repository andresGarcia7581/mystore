package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetRecentProductQueriesUseCase @Inject constructor(private val productRepository: ProductRepository) {

    operator fun invoke(): Flow<List<String>> = productRepository.getRecentProductQueries()
}