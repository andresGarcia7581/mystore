package com.anfegara.mystore.domain.catalog.domain.model.product

data class ProductAttribute(
    val id: String,
    val name: String,
    val value: String?
)