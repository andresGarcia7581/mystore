package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.anfegara.mystore.domain.common.resource.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetProductByIdUseCase @Inject constructor(private val productRepository: ProductRepository) {

    suspend operator fun invoke(id: String): Resource<Product> =
        withContext(Dispatchers.IO) { productRepository.getProductById(id) }
}