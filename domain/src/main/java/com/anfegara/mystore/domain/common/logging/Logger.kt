package com.anfegara.mystore.domain.common.logging

interface Logger {

    fun logException(exception: Throwable)
}