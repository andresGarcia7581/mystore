package com.anfegara.mystore.domain.catalog.readmodel.product

import com.anfegara.mystore.domain.billing.domain.model.payment.Installments
import com.anfegara.mystore.domain.billing.domain.model.payment.Money

data class CatalogItem(
    val id: String,
    val title: String,
    val price: Money,
    val thumbnail: String,
    val installments: Installments?,
    val freeShipping: Boolean,
)