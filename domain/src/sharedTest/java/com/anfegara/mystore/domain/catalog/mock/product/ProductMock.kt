package com.anfegara.mystore.domain.catalog.mock.product

import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.JsonReader

object ProductMock {

    fun getProductCatalog(): List<CatalogItem> =
        JsonReader.getObjectFromJson("productCatalog.json", Array<CatalogItem>::class.java).toList()

    fun getProduct(): Product =
        JsonReader.getObjectFromJson("product.json", Product::class.java)
}