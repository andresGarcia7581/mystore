package com.anfegara.mystore.domain.common

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.File

object JsonReader {

    private val moshi by lazy {
        Moshi.Builder()
            .add(BigDecimalAdapter)
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    fun <T : Any> getObjectFromJson(pathFile: String, clazz: Class<T>): T = checkNotNull(
        moshi.adapter(clazz).fromJson(getJson(pathFile))
    )

    private fun getJson(path: String): String =
        this.javaClass.classLoader.getResource(path)?.let {
            val file = File(it.path)
            String(file.readBytes())
        }.orEmpty()
}