package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

private const val QUERY_EXPECTED_MOCK = "nevera"

@RunWith(MockitoJUnitRunner::class)
class SaveRecentProductQueryUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    private lateinit var saveRecentProductQueryUseCase: SaveRecentProductQueryUseCase

    @Before
    fun setup() {
        saveRecentProductQueryUseCase = SaveRecentProductQueryUseCase(productRepository)
    }

    @Test
    fun `check save recent product query when query is Upper Case`() = runBlocking {
        val query = "NEVERA"

        saveRecentProductQueryUseCase(query)

        verify(productRepository, times(1)).saveRecentProductQuery(QUERY_EXPECTED_MOCK)
    }

    @Test
    fun `check save recent product query when query is Lower Case`() = runBlocking {
        val query = QUERY_EXPECTED_MOCK

        saveRecentProductQueryUseCase(query)

        verify(productRepository, times(1)).saveRecentProductQuery(QUERY_EXPECTED_MOCK)
    }

    @Test
    fun `check save recent product query when query is Capital Case`() = runBlocking {
        val query = "Nevera"

        saveRecentProductQueryUseCase(query)

        verify(productRepository, times(1)).saveRecentProductQuery(QUERY_EXPECTED_MOCK)
    }

    @Test
    fun `check save recent product query when query has blank spaces`() = runBlocking {
        val query = "  nevera  "

        saveRecentProductQueryUseCase(query)

        verify(productRepository, times(1)).saveRecentProductQuery(QUERY_EXPECTED_MOCK)
    }
}