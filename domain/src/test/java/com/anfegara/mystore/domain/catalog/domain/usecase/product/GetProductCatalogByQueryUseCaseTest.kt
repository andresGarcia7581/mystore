package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.mock.product.ProductMock
import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.resource.Resource
import com.anfegara.mystore.domain.common.resource.ResourceError
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class GetProductCatalogByQueryUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    private lateinit var getProductsListByQueryUseCase: GetProductCatalogByQueryUseCase

    @Before
    fun setup() {
        getProductsListByQueryUseCase = GetProductCatalogByQueryUseCase(productRepository)
    }

    @Test
    fun `check get product catalog by query`() = runBlocking {
        val query = "samsung galaxy s6"

        val productCatalog: List<CatalogItem> = ProductMock.getProductCatalog()
        val expectedValue = Resource.Success(productCatalog)

        whenever(productRepository.getProductCatalogByQuery(query)).thenReturn(expectedValue)


        val result = getProductsListByQueryUseCase(query)


        assertEquals(expectedValue, result)
    }

    @Test
    fun `check return a Resource with status error when the request fail`() = runBlocking {
        val query = "Televisor LG"

        val expectedValue = Resource.Error<List<CatalogItem>>(
            "error consultando catálogo",
            ResourceError.Network(message = "error de red", IOException())
        )
        whenever(productRepository.getProductCatalogByQuery(query)).thenReturn(expectedValue)

        val result = getProductsListByQueryUseCase(query)

        assertEquals(expectedValue, result)
    }
}