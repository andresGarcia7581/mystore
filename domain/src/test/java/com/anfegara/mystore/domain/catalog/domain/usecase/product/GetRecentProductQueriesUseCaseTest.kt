package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetRecentProductQueriesUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    private lateinit var getRecentProductQueriesUseCase: GetRecentProductQueriesUseCase

    @Before
    fun setup() {
        getRecentProductQueriesUseCase = GetRecentProductQueriesUseCase(productRepository)
    }

    @Test
    fun `check get product queries`() {

        val expectedValue = flow { emit(listOf("juguetes", "carros", "camisa")) }
        whenever(productRepository.getRecentProductQueries()).thenReturn(expectedValue)

        val result = getRecentProductQueriesUseCase()

        assertEquals(expectedValue, result)
    }
}