package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.mock.product.ProductMock
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.anfegara.mystore.domain.common.resource.Resource
import com.anfegara.mystore.domain.common.resource.ResourceError
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class GetProductByIdUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    private lateinit var getProductByIdUseCase: GetProductByIdUseCase

    @Before
    fun setup() {
        getProductByIdUseCase = GetProductByIdUseCase(productRepository)
    }

    @Test
    fun `check get product by query`() = runBlocking {
        val productId = "MCO608948407"

        val productCatalog: Product = ProductMock.getProduct()
        val expectedValue = Resource.Success(productCatalog)

        whenever(productRepository.getProductById(productId)).thenReturn(expectedValue)


        val result = getProductByIdUseCase(productId)


        assertEquals(expectedValue, result)
    }

    @Test
    fun `check return a Resource with status error when the request fail`() = runBlocking {
        val productId = "MCO728948409"

        val expectedValue = Resource.Error<Product>(
            "error consultando el producto",
            ResourceError.Network(message = "error de red", IOException())
        )
        whenever(productRepository.getProductById(productId)).thenReturn(expectedValue)

        val result = getProductByIdUseCase(productId)

        assertEquals(expectedValue, result)
    }
}