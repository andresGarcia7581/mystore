package com.anfegara.mystore.domain.catalog.domain.usecase.product

import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetRecentProductQueriesByTextUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    private lateinit var getRecentProductQueriesByTextUseCase: GetRecentProductQueriesByTextUseCase

    @Before
    fun setup() {
        getRecentProductQueriesByTextUseCase =
            GetRecentProductQueriesByTextUseCase(productRepository)
    }

    @Test
    fun `check get product queries by text`() {
        val text = "a"

        val expectedValue = flow { emit(listOf("pantalón", "gorra", "blusa")) }
        whenever(productRepository.getRecentProductQueriesByText(text)).thenReturn(expectedValue)

        val result = getRecentProductQueriesByTextUseCase(text)

        assertEquals(expectedValue, result)
    }
}