package com.anfegara.mystore.domain.billing.domain.model.payment

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import java.util.Locale

@RunWith(MockitoJUnitRunner::class)
class MoneyTest {

    private val noBreakingCharacter = "\u00A0"

    @Test
    fun `check money format when locale is different to currency`() {
        val usaLocale = Locale("en", "USA")
        val money = Money(BigDecimal("16900"), CurrencyType.COLOMBIAN_PESO)

        val moneyFormatted = money.toLocalizedString(usaLocale)

        assertEquals("COP16,900", moneyFormatted)
    }

    @Test
    fun `check money format when locale is equal to currency`() {

        val usaLocale = Locale("es", "CO")
        val money = Money(BigDecimal("2300.63"), CurrencyType.COLOMBIAN_PESO)

        val moneyFormatted = money.toLocalizedString(usaLocale)


        assertEquals("\$".plus("${noBreakingCharacter}2.300,63"), moneyFormatted)
    }
}