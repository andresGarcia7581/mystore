package com.anfegara.mystore.domain.billing.domain.model.payment

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertFailsWith

@RunWith(MockitoJUnitRunner::class)
class CurrencyTypeTest {

    @Test
    fun `check return currency type when currency id is valid`() {
        val currencyId = "COP"

        val currencyType = CurrencyType.getCurrencyTypeById(currencyId)

        assertEquals(CurrencyType.COLOMBIAN_PESO, currencyType)
    }

    @Test
    fun `check throw exception when currency id is invalid`() {
        val currencyId = "COi"

        assertFailsWith<IllegalArgumentException> { CurrencyType.getCurrencyTypeById(currencyId) }
    }
}