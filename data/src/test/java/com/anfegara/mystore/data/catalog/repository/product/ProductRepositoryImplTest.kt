package com.anfegara.mystore.data.catalog.repository.product

import com.anfegara.mystore.data.catalog.local.datasource.product.ProductLocalDataSource
import com.anfegara.mystore.data.catalog.remote.datasource.product.ProductRemoteDataSource
import com.anfegara.mystore.domain.catalog.mock.product.ProductMock
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.logging.Logger
import com.anfegara.mystore.domain.common.resource.Resource
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.IllegalArgumentException

@RunWith(MockitoJUnitRunner::class)
class ProductRepositoryImplTest {

    @Mock
    lateinit var productRemoteDataSource: ProductRemoteDataSource

    @Mock
    lateinit var productLocalDataSource: ProductLocalDataSource

    @Mock
    lateinit var logger: Logger

    private lateinit var productRepository: ProductRepositoryImpl

    @Before
    fun setup() {
        productRepository =
            ProductRepositoryImpl(productRemoteDataSource, productLocalDataSource, logger)
    }

    @Test
    fun `check get product catalog by query`() = runBlocking {
        val query = "lavadora lg"

        val productCatalog = ProductMock.getProductCatalog()
        val expectedValue = Resource.Success(productCatalog)

        whenever(productRemoteDataSource.getProductCatalogByQuery(query)).thenReturn(productCatalog)


        val result = productRepository.getProductCatalogByQuery(query)


        assertEquals(expectedValue, result)
    }

    @Test
    fun `check get a product catalog by query return a Resource with status error when the request fail`() =
        runBlocking {
            val query = "Ceular motorola"

            val cause = IllegalArgumentException("Error procesando la solicitud")
            val expectedValue = Resource.Error<CatalogItem>(
                message = "Error consultando los productos.",
                error = cause
            )

            whenever(productRemoteDataSource.getProductCatalogByQuery(query)).thenThrow(cause)

            val result = productRepository.getProductCatalogByQuery(query)

            assertEquals(expectedValue, result)
        }

    @Test
    fun `check get product by id`() = runBlocking {
        val productId = "MCO608948403"

        val product = ProductMock.getProduct()
        val expectedValue = Resource.Success(product)

        whenever(productRemoteDataSource.getProductById(productId)).thenReturn(product)

        val result = productRepository.getProductById(productId)

        assertEquals(expectedValue, result)
    }

    @Test
    fun `check get a product by id return a Resource with status error when the request fail`() =
        runBlocking {
            val productId = "MCO608948403"

            val cause = IllegalArgumentException("Error procesando la solicitud")
            val expectedValue = Resource.Error<CatalogItem>(
                message = "Error consultando el producto.",
                error = cause
            )

            whenever(productRemoteDataSource.getProductById(productId)).thenThrow(cause)

            val result = productRepository.getProductById(productId)

            assertEquals(expectedValue, result)
        }

    @Test
    fun `check save recent product query`() = runBlocking {
        val query = "Estufa"

        productRepository.saveRecentProductQuery(query)

        verify(productLocalDataSource, times(1)).saveRecentProductQuery(query)
    }


    @Test
    fun `check get recent product queries`() {

        val expectedValue = flow { emit(listOf("carrito", "muñeca")) }
        whenever(productLocalDataSource.getRecentProductQueries()).thenReturn(expectedValue)

        val recentQueries = productRepository.getRecentProductQueries()

        assertEquals(expectedValue, recentQueries)
    }

    @Test
    fun `check get recent product queries by text`() {
        val text = "ba"

        val expectedValue = flow { emit(listOf("balón", "bañera", "batata")) }
        whenever(productLocalDataSource.getRecentProductQueriesByText(text)).thenReturn(expectedValue)

        val recentQueries = productRepository.getRecentProductQueriesByText(text)

        assertEquals(expectedValue, recentQueries)
    }
}