package com.anfegara.mystore.data.catalog.remote.datasource.product

import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem

interface ProductRemoteDataSource {

    suspend fun getProductCatalogByQuery(query: String): List<CatalogItem>

    suspend fun getProductById(id: String): Product
}