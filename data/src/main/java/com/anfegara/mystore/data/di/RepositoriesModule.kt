package com.anfegara.mystore.data.di

import com.anfegara.mystore.data.catalog.repository.product.ProductRepositoryImpl
import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoriesModule {

    @Binds
    @Singleton
    abstract fun bindProductRepository(productRepositoryImpl: ProductRepositoryImpl): ProductRepository
}