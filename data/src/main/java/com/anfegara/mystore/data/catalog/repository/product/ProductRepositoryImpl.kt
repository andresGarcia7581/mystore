package com.anfegara.mystore.data.catalog.repository.product

import com.anfegara.mystore.data.catalog.local.datasource.product.ProductLocalDataSource
import com.anfegara.mystore.data.catalog.remote.datasource.product.ProductRemoteDataSource
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.domain.repository.product.ProductRepository
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.logging.Logger
import com.anfegara.mystore.domain.common.resource.Resource
import kotlinx.coroutines.flow.Flow
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

private const val GET_PRODUCT_CATALOG_ERROR_MESSAGE = "Error consultando los productos."
private const val GET_PRODUCT_BY_ID_ERROR_MESSAGE = "Error consultando el producto."

@Singleton
class ProductRepositoryImpl @Inject constructor(
    private val remoteDataSource: ProductRemoteDataSource,
    private val localDataSource: ProductLocalDataSource,
    private val logger: Logger
) : ProductRepository {

    override suspend fun getProductCatalogByQuery(query: String): Resource<List<CatalogItem>> =
        try {
            val products = remoteDataSource.getProductCatalogByQuery(query)
            Resource.Success(products)
        } catch (exception: Exception) {
            logger.logException(exception)
            Resource.Error(GET_PRODUCT_CATALOG_ERROR_MESSAGE, exception)
        }

    override suspend fun getProductById(id: String): Resource<Product> = try {
        val products = remoteDataSource.getProductById(id)
        Resource.Success(products)
    } catch (exception: Exception) {
        logger.logException(exception)
        Resource.Error(GET_PRODUCT_BY_ID_ERROR_MESSAGE, exception)
    }

    override suspend fun saveRecentProductQuery(query: String) =
        localDataSource.saveRecentProductQuery(query)

    override fun getRecentProductQueries(): Flow<List<String>> =
        localDataSource.getRecentProductQueries()

    override fun getRecentProductQueriesByText(text: String): Flow<List<String>> = localDataSource.getRecentProductQueriesByText(text)
}