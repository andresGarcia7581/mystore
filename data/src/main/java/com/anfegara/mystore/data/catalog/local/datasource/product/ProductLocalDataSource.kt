package com.anfegara.mystore.data.catalog.local.datasource.product

import kotlinx.coroutines.flow.Flow

interface ProductLocalDataSource {

    suspend fun saveRecentProductQuery(query: String)

    fun getRecentProductQueries(): Flow<List<String>>

    fun getRecentProductQueriesByText(text: String): Flow<List<String>>
}