package com.anfegara.mystore.infraestructure.catalog.remote.mapper.product

import com.anfegara.mystore.domain.billing.domain.model.payment.CurrencyType
import com.anfegara.mystore.domain.billing.domain.model.payment.Money
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.infraestructure.billing.remote.mapper.payment.InstallmentsMapper
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.CatalogItemDTO
import javax.inject.Inject

class CatalogMapper @Inject constructor(private val installmentsMapper: InstallmentsMapper) {
    fun mapDTOToEntity(catalogItemDTO: CatalogItemDTO) = CatalogItem(
        id = catalogItemDTO.id,
        title = catalogItemDTO.title,
        price = Money(catalogItemDTO.price, CurrencyType.getCurrencyTypeById(catalogItemDTO.currencyId)),
        thumbnail = catalogItemDTO.thumbnail,
        freeShipping = catalogItemDTO.shipping.free,
        installments = catalogItemDTO.installments?.let { installmentsMapper.mapDTOToEntity(it) },
    )
}