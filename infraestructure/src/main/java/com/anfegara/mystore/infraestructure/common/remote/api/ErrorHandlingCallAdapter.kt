package com.anfegara.mystore.infraestructure.common.remote.api

import retrofit2.Call
import retrofit2.CallAdapter

class ErrorHandlingCallAdapter<R : Any>(
    private val delegateAdapter: CallAdapter<R, Call<R>>
) : CallAdapter<R, Call<R>> by delegateAdapter {

    override fun adapt(call: Call<R>): Call<R> =
        delegateAdapter.adapt(CallWithErrorHandling(call))
}