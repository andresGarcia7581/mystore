package com.anfegara.mystore.infraestructure.catalog.remote.datasource.product

import com.anfegara.mystore.data.catalog.remote.datasource.product.ProductRemoteDataSource
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.ProductApi
import com.anfegara.mystore.infraestructure.catalog.remote.mapper.product.CatalogMapper
import com.anfegara.mystore.infraestructure.catalog.remote.mapper.product.ProductMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRemoteRetrofitDataSource @Inject constructor(
    private val productApi: ProductApi,
    private val catalogMapper: CatalogMapper,
    private val productMapper: ProductMapper,
) : ProductRemoteDataSource {

    override suspend fun getProductCatalogByQuery(query: String): List<CatalogItem> {
        val products = productApi.getProductCatalogByQuery(query = query)
        return products.results.map { catalogMapper.mapDTOToEntity(it) }
    }

    override suspend fun getProductById(id: String): Product {
        val product = productApi.getProductById(id)
        val productDescription = productApi.getProductDescription(id)
        return productMapper.mapDTOToEntity(product, productDescription)
    }
}