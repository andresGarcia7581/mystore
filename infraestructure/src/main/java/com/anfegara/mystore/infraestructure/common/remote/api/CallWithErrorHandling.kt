package com.anfegara.mystore.infraestructure.common.remote.api

import com.anfegara.mystore.domain.common.resource.ResourceError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.HttpURLConnection

private const val NETWORK_ERROR_MESSAGE: String = "Error de red"
private const val RESOURCE_NOT_FOUND_MESSAGE: String = "No se encuentra el recurso solicitado"
private const val UNEXPECTED_ERROR_MESSAGE: String = "Error procesando la solicitud"
private const val ACCESS_DENIED_MESSAGE: String = "No está autorizado"

class CallWithErrorHandling<R: Any>(private val delegate: Call<R>) : Call<R> by delegate {

    override fun enqueue(callback: Callback<R>) {
        delegate.enqueue(object : Callback<R> {
            override fun onResponse(call: Call<R>, response: Response<R>) {
                if (response.isSuccessful) {
                    callback.onResponse(call, response)
                } else {
                    val resourceError = getResourceErrorFromHttpException(HttpException(response))
                    callback.onFailure(call, resourceError)
                }
            }

            override fun onFailure(call: Call<R>, exception: Throwable) {
                val resourceError = getResourceErrorFromException(exception)
                callback.onFailure(call, resourceError)
            }
        })
    }

    override fun clone() = CallWithErrorHandling(delegate.clone())

    private fun getResourceErrorFromException(exception: Throwable): ResourceError =
        if (exception is IOException) {
            ResourceError.Network(NETWORK_ERROR_MESSAGE, exception)
        } else ResourceError.Unexpected(UNEXPECTED_ERROR_MESSAGE, exception)

    private fun getResourceErrorFromHttpException(exception: HttpException): ResourceError =
        when (exception.code()) {
            HttpURLConnection.HTTP_NOT_FOUND -> ResourceError.NotFound(RESOURCE_NOT_FOUND_MESSAGE, exception)
            HttpURLConnection.HTTP_UNAUTHORIZED -> ResourceError.AccessDenied(ACCESS_DENIED_MESSAGE, exception)
            else -> ResourceError.Unexpected(UNEXPECTED_ERROR_MESSAGE, exception)
        }
}