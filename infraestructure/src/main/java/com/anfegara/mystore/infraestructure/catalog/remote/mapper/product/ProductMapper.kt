package com.anfegara.mystore.infraestructure.catalog.remote.mapper.product

import com.anfegara.mystore.domain.billing.domain.model.payment.CurrencyType
import com.anfegara.mystore.domain.billing.domain.model.payment.Money
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductDTO
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductDescriptionDTO
import com.anfegara.mystore.infraestructure.sales.remote.mapper.seller.SellerMapper
import com.anfegara.mystore.infraestructure.shipping.remote.mapper.shipping.ShippingMapper
import javax.inject.Inject

class ProductMapper @Inject constructor(
    private val productAttributeMapper: ProductAttributeMapper,
    private val shippingMapper: ShippingMapper,
    private val sellerMapper: SellerMapper
) {

    fun mapDTOToEntity(product: ProductDTO, productDescription: ProductDescriptionDTO) = Product(
        id = product.id,
        name = product.title,
        description = productDescription.plainText,
        images = product.images.map { it.url },
        price = Money(product.price, CurrencyType.getCurrencyTypeById(product.currencyId)),
        soldQuantity = product.soldQuantity,
        availableQuantity = product.availableQuantity,
        warranty = product.warranty,
        attributes = product.attributes.map { productAttributeMapper.mapDTOToEntity(it) },
        shipping = shippingMapper.mapDTOToEntity(product.shipping),
        seller = sellerMapper.mapDTOToEntity(product.sellerId, product.sellerAddress)
    )
}