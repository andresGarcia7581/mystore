package com.anfegara.mystore.infraestructure.catalog.local.entity.product

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "recent_product_query", indices = [Index(value = ["text"], unique = true)])
data class RecentProductQuery(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val text: String,
    val createdDate: Long = System.currentTimeMillis()
)