package com.anfegara.mystore.infraestructure.person.remote.mapper.locationdata

import com.anfegara.mystore.domain.person.domain.model.locationdata.Address
import com.anfegara.mystore.infraestructure.person.remote.api.locationdata.model.AddressDTO
import javax.inject.Inject

class AddressMapper @Inject constructor() {

    fun mapDTOToEntity(address: AddressDTO) = Address(
        stateName = address.state.name,
        cityName = address.city.name
    )
}