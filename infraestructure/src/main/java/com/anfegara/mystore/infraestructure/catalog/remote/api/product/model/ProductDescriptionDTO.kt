package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.squareup.moshi.Json

data class ProductDescriptionDTO(
    @Json(name = "plain_text") val plainText: String
)