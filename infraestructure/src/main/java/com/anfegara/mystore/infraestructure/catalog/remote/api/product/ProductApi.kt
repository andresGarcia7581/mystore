package com.anfegara.mystore.infraestructure.catalog.remote.api.product

import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductCatalogDTO
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductDTO
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductDescriptionDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val SITE_ID = "site-id"
private const val SITE_ID_COLOMBIA = "MCO"
private const val QUERY_PARAMETER = "q"
private const val PRODUCT_ID = "id"

interface ProductApi {

    @GET("sites/{$SITE_ID}/search")
    suspend fun getProductCatalogByQuery(
        @Path(SITE_ID) site: String = SITE_ID_COLOMBIA,
        @Query(QUERY_PARAMETER) query: String
    ): ProductCatalogDTO

    @GET("items/{$PRODUCT_ID}")
    suspend fun getProductById(@Path(PRODUCT_ID) productId: String): ProductDTO

    @GET("items/{$PRODUCT_ID}/description")
    suspend fun getProductDescription(@Path(PRODUCT_ID) productId: String): ProductDescriptionDTO
}