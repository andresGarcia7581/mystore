package com.anfegara.mystore.infraestructure.person.remote.api.locationdata.model

import com.squareup.moshi.Json

data class PlaceDTO(
    @Json(name = "id") val id: String?,
    @Json(name = "name") val name: String
)