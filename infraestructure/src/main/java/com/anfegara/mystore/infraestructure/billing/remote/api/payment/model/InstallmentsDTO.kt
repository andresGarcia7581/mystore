package com.anfegara.mystore.infraestructure.billing.remote.api.payment.model

import com.squareup.moshi.Json
import java.math.BigDecimal

data class InstallmentsDTO(
    @Json(name = "quantity") val quantity: Int,
    @Json(name = "amount") val amount: BigDecimal,
    @Json(name = "rate") val rate: BigDecimal,
    @Json(name = "currency_id") val currencyId: String
)