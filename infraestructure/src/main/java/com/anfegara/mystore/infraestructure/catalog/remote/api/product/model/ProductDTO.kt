package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.anfegara.mystore.infraestructure.person.remote.api.locationdata.model.AddressDTO
import com.anfegara.mystore.infraestructure.shipping.remote.api.shipping.model.ShippingDTO
import com.squareup.moshi.Json
import java.math.BigDecimal

class ProductDTO(
    @Json(name = "id") val id: String,
    @Json(name = "title") val title: String,
    @Json(name = "pictures") val images: List<ImageDTO>,
    @Json(name = "price") val price: BigDecimal,
    @Json(name = "currency_id") val currencyId: String,
    @Json(name = "sold_quantity") val soldQuantity: Int,
    @Json(name = "available_quantity") val availableQuantity: Int,
    @Json(name = "warranty") val warranty: String?,
    @Json(name = "attributes") val attributes: List<ProductAttributeDTO>,
    @Json(name = "seller_id") val sellerId: String,
    @Json(name = "seller_address") val sellerAddress: AddressDTO,
    @Json(name = "shipping") val shipping: ShippingDTO
)