package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.squareup.moshi.Json

data class ImageDTO(
    @Json(name = "secure_url") val url: String
)