package com.anfegara.mystore.infraestructure.catalog.local.datasource.product

import com.anfegara.mystore.data.catalog.local.datasource.product.ProductLocalDataSource
import com.anfegara.mystore.infraestructure.catalog.local.dao.product.RecentProductQueryDao
import com.anfegara.mystore.infraestructure.catalog.local.entity.product.RecentProductQuery
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductLocalRoomDataSource @Inject constructor(private val recentQueryDao: RecentProductQueryDao) :
    ProductLocalDataSource {

    override suspend fun saveRecentProductQuery(query: String) =
        recentQueryDao.insert(RecentProductQuery(text = query))

    override fun getRecentProductQueries(): Flow<List<String>> = recentQueryDao.query()

    override fun getRecentProductQueriesByText(text: String): Flow<List<String>> =
        recentQueryDao.queryByText(text)
}