package com.anfegara.mystore.infraestructure.common.remote.api

import com.anfegara.mystore.infraestructure.common.remote.converter.BigDecimalAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

private const val BASE_URL = "https://api.mercadolibre.com/"

@Singleton
class ApiProvider @Inject constructor() {

    private val moshi by lazy {
        Moshi.Builder()
            .add(BigDecimalAdapter)
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(ErrorHandlingCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    fun <T> create(clazz: Class<T>): T = retrofit.create(clazz)
}