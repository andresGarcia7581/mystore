package com.anfegara.mystore.infraestructure.di.local

import android.app.Application
import androidx.room.Room
import com.anfegara.mystore.infraestructure.catalog.local.dao.product.RecentProductQueryDao
import com.anfegara.mystore.infraestructure.common.local.database.MyStoreDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

private const val DATA_BASE_NAME = "mystoredb"

@Module
@InstallIn(SingletonComponent::class)
object PersistenceModule {

    @Provides
    @Singleton
    fun providesDataBase(application: Application): MyStoreDataBase {
        return Room
            .databaseBuilder(application, MyStoreDataBase::class.java, DATA_BASE_NAME)
            .build()
    }

    @Provides
    fun providesRecentProductQueryDAO(database: MyStoreDataBase): RecentProductQueryDao =
        database.getRecentProductQueryDao()
}