package com.anfegara.mystore.infraestructure.sales.remote.mapper.seller

import com.anfegara.mystore.domain.sales.domain.model.seller.Seller
import com.anfegara.mystore.infraestructure.person.remote.api.locationdata.model.AddressDTO
import com.anfegara.mystore.infraestructure.person.remote.mapper.locationdata.AddressMapper
import javax.inject.Inject

class SellerMapper @Inject constructor(private val addressMapper: AddressMapper) {

    fun mapDTOToEntity(sellerId: String, sellerAddress: AddressDTO) = Seller(
        id = sellerId,
        address = addressMapper.mapDTOToEntity(sellerAddress)
    )
}