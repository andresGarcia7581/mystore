package com.anfegara.mystore.infraestructure.shipping.remote.api.shipping.model

import com.squareup.moshi.Json

class ShippingDTO(
    @Json(name = "mode") val mode: String,
    @Json(name = "free_shipping") val free: Boolean
)