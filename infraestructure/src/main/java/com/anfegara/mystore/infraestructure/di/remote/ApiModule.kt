package com.anfegara.mystore.infraestructure.di.remote

import com.anfegara.mystore.infraestructure.catalog.remote.api.product.ProductApi
import com.anfegara.mystore.infraestructure.common.remote.api.ApiProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun provideProductApi(apiProvider: ApiProvider): ProductApi = apiProvider.create(ProductApi::class.java)
}