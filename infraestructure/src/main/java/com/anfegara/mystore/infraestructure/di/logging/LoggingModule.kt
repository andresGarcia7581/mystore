package com.anfegara.mystore.infraestructure.di.logging

import com.anfegara.mystore.domain.common.logging.Logger
import com.anfegara.mystore.infraestructure.common.logging.FirebaseCrashlyticsLogger
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LoggingModule {

    @Binds
    @Singleton
    abstract fun bindFirebaseCrashlyticsLogger(firebaseCrashlyticsLogger: FirebaseCrashlyticsLogger): Logger
}