package com.anfegara.mystore.infraestructure.person.remote.api.locationdata.model

import com.squareup.moshi.Json

data class AddressDTO(
    @Json(name = "city") val city: PlaceDTO,
    @Json(name = "state") val state: PlaceDTO
)