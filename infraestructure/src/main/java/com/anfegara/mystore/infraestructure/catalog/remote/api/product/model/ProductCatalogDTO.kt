package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.squareup.moshi.Json

data class ProductCatalogDTO(
    @Json(name = "results") val results: List<CatalogItemDTO>
)