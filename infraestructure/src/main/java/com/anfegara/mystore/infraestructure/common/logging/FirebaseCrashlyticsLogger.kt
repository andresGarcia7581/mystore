package com.anfegara.mystore.infraestructure.common.logging

import com.anfegara.mystore.domain.common.logging.Logger
import com.google.firebase.crashlytics.FirebaseCrashlytics
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FirebaseCrashlyticsLogger @Inject constructor() : Logger {

    private val loggerInstance by lazy { FirebaseCrashlytics.getInstance() }

    override fun logException(exception: Throwable) = loggerInstance.recordException(exception)
}