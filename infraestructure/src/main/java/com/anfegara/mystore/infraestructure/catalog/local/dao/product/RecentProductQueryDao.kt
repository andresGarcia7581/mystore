package com.anfegara.mystore.infraestructure.catalog.local.dao.product

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.anfegara.mystore.infraestructure.catalog.local.entity.product.RecentProductQuery
import kotlinx.coroutines.flow.Flow

@Dao
interface RecentProductQueryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: RecentProductQuery)

    @Query("SELECT text FROM recent_product_query ORDER BY createdDate DESC")
    fun query(): Flow<List<String>>

    @Query("SELECT text FROM recent_product_query WHERE text LIKE '%'|| :query ||'%' ORDER BY createdDate DESC")
    fun queryByText(query: String): Flow<List<String>>
}