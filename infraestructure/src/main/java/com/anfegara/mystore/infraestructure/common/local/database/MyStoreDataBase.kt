package com.anfegara.mystore.infraestructure.common.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.anfegara.mystore.infraestructure.catalog.local.entity.product.RecentProductQuery
import com.anfegara.mystore.infraestructure.catalog.local.dao.product.RecentProductQueryDao

@Database(entities = [RecentProductQuery::class], version = 1)
abstract class MyStoreDataBase : RoomDatabase() {
    abstract fun getRecentProductQueryDao(): RecentProductQueryDao
}