package com.anfegara.mystore.infraestructure.common.remote.api

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

private const val VALID_NUMBER_ARGUMENTS_GENERIC_TYPE = 1

class ErrorHandlingCallAdapterFactory : CallAdapter.Factory() {

    @Suppress("UNCHECKED_CAST")
    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): CallAdapter<Any, Call<Any>>? {
        return if (isAdapterCanHandleCall(returnType)) {
            val delegate = retrofit.nextCallAdapter(this, returnType, annotations)
            val delegateAdapter = delegate as CallAdapter<Any, Call<Any>>
            ErrorHandlingCallAdapter(delegateAdapter = delegateAdapter)
        } else null
    }

    private fun isAdapterCanHandleCall(returnType: Type): Boolean = returnType.run {
        val isSuspendFunctionWrapper = getRawType(this) == Call::class.java
        val isGenericWithOneArgument = this is ParameterizedType && this.actualTypeArguments.size == VALID_NUMBER_ARGUMENTS_GENERIC_TYPE
        isSuspendFunctionWrapper && isGenericWithOneArgument
    }
}