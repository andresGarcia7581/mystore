package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.anfegara.mystore.infraestructure.billing.remote.api.payment.model.InstallmentsDTO
import com.anfegara.mystore.infraestructure.shipping.remote.api.shipping.model.ShippingDTO
import com.squareup.moshi.Json
import java.math.BigDecimal

data class CatalogItemDTO(
    @Json(name = "id") val id: String,
    @Json(name = "title") val title: String,
    @Json(name = "price") val price: BigDecimal,
    @Json(name = "currency_id") val currencyId: String,
    @Json(name = "thumbnail") val thumbnail: String,
    @Json(name = "installments")  val installments: InstallmentsDTO?,
    @Json(name = "shipping") val shipping: ShippingDTO
)