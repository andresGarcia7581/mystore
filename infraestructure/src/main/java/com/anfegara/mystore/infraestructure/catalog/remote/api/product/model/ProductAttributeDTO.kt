package com.anfegara.mystore.infraestructure.catalog.remote.api.product.model

import com.squareup.moshi.Json

data class ProductAttributeDTO(
    @Json(name = "id") val id: String,
    @Json(name = "name") val name: String,
    @Json(name = "value_name") val value: String?
)