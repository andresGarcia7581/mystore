package com.anfegara.mystore.infraestructure.catalog.remote.mapper.product

import com.anfegara.mystore.domain.catalog.domain.model.product.ProductAttribute
import com.anfegara.mystore.infraestructure.catalog.remote.api.product.model.ProductAttributeDTO
import javax.inject.Inject

class ProductAttributeMapper @Inject constructor() {

    fun mapDTOToEntity(productAttribute: ProductAttributeDTO) = ProductAttribute(
        id = productAttribute.id,
        name = productAttribute.name,
        value = productAttribute.value
    )
}