package com.anfegara.mystore.infraestructure.di.remote

import com.anfegara.mystore.data.catalog.remote.datasource.product.ProductRemoteDataSource
import com.anfegara.mystore.infraestructure.catalog.remote.datasource.product.ProductRemoteRetrofitDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteDataSourceModule {

    @Binds
    @Singleton
    abstract fun bindProductRemoteDataSource(productRemoteRetrofitDataSource: ProductRemoteRetrofitDataSource): ProductRemoteDataSource
}