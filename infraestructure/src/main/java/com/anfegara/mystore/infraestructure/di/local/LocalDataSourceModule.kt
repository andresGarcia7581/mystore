package com.anfegara.mystore.infraestructure.di.local

import com.anfegara.mystore.data.catalog.local.datasource.product.ProductLocalDataSource
import com.anfegara.mystore.infraestructure.catalog.local.datasource.product.ProductLocalRoomDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalDataSourceModule {
    @Binds
    @Singleton
    abstract fun bindProductRemoteDataSource(productLocalRoomDataSource: ProductLocalRoomDataSource): ProductLocalDataSource
}