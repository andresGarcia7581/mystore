package com.anfegara.mystore.infraestructure.billing.remote.mapper.payment

import com.anfegara.mystore.domain.billing.domain.model.payment.CurrencyType
import com.anfegara.mystore.domain.billing.domain.model.payment.Installments
import com.anfegara.mystore.domain.billing.domain.model.payment.Money
import com.anfegara.mystore.infraestructure.billing.remote.api.payment.model.InstallmentsDTO
import javax.inject.Inject

class InstallmentsMapper @Inject constructor() {

    fun mapDTOToEntity(installmentsDTO: InstallmentsDTO) = Installments(
        quantity = installmentsDTO.quantity,
        amount = Money(installmentsDTO.amount, CurrencyType.getCurrencyTypeById(installmentsDTO.currencyId)),
        rate = installmentsDTO.rate
    )
}