package com.anfegara.mystore.infraestructure.shipping.remote.mapper.shipping

import com.anfegara.mystore.domain.shipping.domain.model.shipping.Shipping
import com.anfegara.mystore.infraestructure.shipping.remote.api.shipping.model.ShippingDTO
import javax.inject.Inject

class ShippingMapper @Inject constructor() {

    fun mapDTOToEntity(shipping: ShippingDTO) = Shipping(
        mode = shipping.mode,
        free = shipping.free
    )
}