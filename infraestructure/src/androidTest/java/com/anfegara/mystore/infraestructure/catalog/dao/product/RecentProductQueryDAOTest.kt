package com.anfegara.mystore.infraestructure.catalog.dao.product

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.anfegara.mystore.infraestructure.catalog.local.dao.product.RecentProductQueryDao
import com.anfegara.mystore.infraestructure.catalog.local.entity.product.RecentProductQuery
import com.anfegara.mystore.infraestructure.common.local.database.MyStoreDataBase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RecentProductQueryDAOTest {
    private lateinit var recentQueryDao: RecentProductQueryDao
    private lateinit var db: MyStoreDataBase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, MyStoreDataBase::class.java
        ).build()
        recentQueryDao = db.getRecentProductQueryDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun getRecentProductQueriesTest() = runBlocking {
        val firstQuery = "bañera"
        val secondQuery = "carro"
        recentQueryDao.insert(RecentProductQuery(text = firstQuery))
        recentQueryDao.insert(RecentProductQuery(text = secondQuery))

        val queries = recentQueryDao.query().first()

        assertEquals(listOf(secondQuery, firstQuery), queries)
    }

    @Test
    fun getRecentProductQueriesByTextTest() = runBlocking {
        val text = "va"
        val firstQuery = "vajilla"
        val secondQuery = "vacuna"
        val thirdQuery = "vitrina"

        recentQueryDao.insert(RecentProductQuery(text = firstQuery))
        recentQueryDao.insert(RecentProductQuery(text = secondQuery))
        recentQueryDao.insert(RecentProductQuery(text = thirdQuery))

        val queries = recentQueryDao.queryByText(text).first()

        assertEquals(listOf(secondQuery, firstQuery), queries)
    }
}