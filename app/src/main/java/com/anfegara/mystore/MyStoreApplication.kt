package com.anfegara.mystore

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyStoreApplication : MultiDexApplication()