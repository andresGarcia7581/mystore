package com.anfegara.mystore.di

import com.anfegara.mystore.data.di.RepositoriesModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [RepositoriesModule::class])
@InstallIn(SingletonComponent::class)
object AppModule