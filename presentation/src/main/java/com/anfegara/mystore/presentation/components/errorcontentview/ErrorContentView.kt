package com.anfegara.mystore.presentation.components.errorcontentview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.anfegara.mystore.presentation.common.ErrorContent
import com.anfegara.mystore.presentation.common.TextResource
import com.anfegara.mystore.presentation.databinding.ErrorContentViewBinding
import com.anfegara.mystore.presentation.extensions.asString

class ErrorContentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr, defStyleRes) {

    private val binding: ErrorContentViewBinding =
        ErrorContentViewBinding.inflate(LayoutInflater.from(context), this, true)

    fun setUp(errorContent: ErrorContent) {
        loadUI(errorContent)
        visibility = View.VISIBLE
    }

    private fun loadUI(errorContent: ErrorContent) {
        showImage(errorContent.idResourceImage)
        showTitle(errorContent.title)
        showAdvice(errorContent.advice)
        errorContent.action?.let { setUpAction(it) } ?: resetAction()
    }

    private fun showImage(image: Int) {
        binding.errorIcon.setImageResource(image)
    }

    private fun showTitle(title: TextResource) {
        binding.errorTitle.text = title.asString(resources)
    }

    private fun showAdvice(advice: TextResource) {
        binding.errorAdvice.text = advice.asString(resources)
    }

    private fun resetAction() {
        binding.actionButton.apply {
            text = null
            setOnClickListener(null)
            visibility = View.GONE
        }
    }

    private fun setUpAction(action: Pair<String, () -> Unit>) {
        binding.actionButton.apply {
            text = action.first
            setOnClickListener { action.second.invoke() }
            visibility = View.VISIBLE
        }
    }
}