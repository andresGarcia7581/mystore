package com.anfegara.mystore.presentation.views.prductsearch.productcatalog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anfegara.mystore.domain.catalog.domain.usecase.product.GetProductCatalogByQueryUseCase
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.resource.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException
import javax.inject.Inject

private const val QUERY_ARGUMENT = "query"

@HiltViewModel
class ProductCatalogViewModel @Inject constructor(
    private val getProductCatalogByQueryUseCase: GetProductCatalogByQueryUseCase,
    savedState: SavedStateHandle
) : ViewModel() {

    private val query: String = savedState.get<String>(QUERY_ARGUMENT)
        ?: throw IllegalArgumentException("query is required")

    private val _productCatalogState: MutableLiveData<Resource<List<CatalogItem>>> by lazy {
        MutableLiveData<Resource<List<CatalogItem>>>().also { getProductCatalogByQuery(query) }
    }

    val productCatalogState: LiveData<Resource<List<CatalogItem>>> = _productCatalogState

    fun getProductCatalogByQuery(query: String = this.query) {
        viewModelScope.launch(Dispatchers.Main) {
            _productCatalogState.value = Resource.Loading()
            _productCatalogState.value = getProductCatalogByQueryUseCase.invoke(query)
        }
    }
}