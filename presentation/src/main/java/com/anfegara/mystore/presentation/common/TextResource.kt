package com.anfegara.mystore.presentation.common

import androidx.annotation.StringRes

sealed class TextResource {

    data class SimpleTextResource(
        val text: String
    ) : TextResource()

    data class IdTextResource(
        @StringRes val id: Int
    ) : TextResource()

    companion object {
        fun fromText(text: String): TextResource = SimpleTextResource(text)
        fun fromStringId(@StringRes id: Int): TextResource = IdTextResource(id)
    }
}