package com.anfegara.mystore.presentation.adapters.productsearch.productdetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.anfegara.mystore.domain.catalog.domain.model.product.ProductAttribute
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.databinding.ProductAttributeViewBinding

private const val BASE_EVEN_NUMBER = 2

class ProductAttributesAdapter(private val productAttributes: List<ProductAttribute>) :
    RecyclerView.Adapter<ProductAttributesAdapter.ProductAttributeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductAttributeViewHolder = ProductAttributeViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.product_attribute_view, parent, false)
    )

    override fun onBindViewHolder(
        holder: ProductAttributeViewHolder,
        position: Int
    ) {
        holder.bind(productAttributes[position])
    }


    override fun getItemCount(): Int = productAttributes.size

    class ProductAttributeViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ProductAttributeViewBinding.bind(view)

        fun bind(productAttribute: ProductAttribute) {
            binding.attributeName.text = productAttribute.name
            showProductAttributeValue(productAttribute.value)
            setRowBackGround()
        }

        private fun showProductAttributeValue(value: String?) {
            val valueLabel =
                if (value.isNullOrBlank()) view.context.getString(R.string.unspecified_attribute_label) else value
            binding.attributeValue.text = valueLabel
        }

        private fun setRowBackGround() {
            val color = if (isEvenRow()) R.color.gray_300 else R.color.gray_80_opacity
            binding.root.setBackgroundColor(ContextCompat.getColor(view.context, color))
        }

        private fun isEvenRow() = adapterPosition % BASE_EVEN_NUMBER == 0
    }
}