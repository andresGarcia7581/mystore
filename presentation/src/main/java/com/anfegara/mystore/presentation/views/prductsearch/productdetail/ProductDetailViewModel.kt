package com.anfegara.mystore.presentation.views.prductsearch.productdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.domain.usecase.product.GetProductByIdUseCase
import com.anfegara.mystore.domain.common.resource.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException
import javax.inject.Inject

private const val PRODUCT_ID_ARGUMENT = "productId"

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val getProductByIdUseCase: GetProductByIdUseCase,
    savedState: SavedStateHandle
) : ViewModel() {

    private val productId: String = savedState.get<String>(PRODUCT_ID_ARGUMENT) ?: throw IllegalArgumentException("product id is required")

    private val _product: MutableLiveData<Resource<Product>> by lazy {
        MutableLiveData<Resource<Product>>().also { getProductById(productId) }
    }

    val product: LiveData<Resource<Product>> = _product

    fun getProductById(id: String) {
        viewModelScope.launch(Dispatchers.Main) {
            _product.value = Resource.Loading()
            _product.value = getProductByIdUseCase.invoke(id)
        }
    }
}