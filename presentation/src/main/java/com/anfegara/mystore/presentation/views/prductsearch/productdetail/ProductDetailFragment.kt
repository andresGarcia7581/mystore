package com.anfegara.mystore.presentation.views.prductsearch.productdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.anfegara.mystore.domain.catalog.domain.model.product.Product
import com.anfegara.mystore.domain.catalog.domain.model.product.ProductAttribute
import com.anfegara.mystore.domain.common.resource.Resource
import com.anfegara.mystore.domain.common.resource.ResourceError
import com.anfegara.mystore.domain.sales.domain.model.seller.Seller
import com.anfegara.mystore.domain.shipping.domain.model.shipping.Shipping
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.adapters.productsearch.productdetail.ProductAttributesAdapter
import com.anfegara.mystore.presentation.common.ErrorContent
import com.anfegara.mystore.presentation.databinding.FragmentProductDetailBinding
import com.anfegara.mystore.presentation.databinding.ProductDetailContentBinding
import com.anfegara.mystore.presentation.extensions.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private lateinit var binding: FragmentProductDetailBinding
    private lateinit var productDetailContentBinding: ProductDetailContentBinding
    private val args: ProductDetailFragmentArgs by navArgs()
    private val viewModel: ProductDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        productDetailContentBinding = binding.productDetailContent
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolBar()
        setUpProductDetail()
    }

    private fun setUpToolBar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
    }

    private fun setUpProductDetail() {
        viewModel.product.observe(viewLifecycleOwner) { handleProductState(it) }
    }

    private fun handleProductState(productState: Resource<Product>) {
        resetInitialView()
        when (productState) {
            is Resource.Loading -> showProgress()
            is Resource.Error -> handleError(productState.error)
            is Resource.Success -> showProductDetail(productState.data)
        }
    }

    private fun resetInitialView() {
        hideProgress()
        hideErrorView()
    }

    private fun hideProgress() {
        binding.progressBar.visibility = GONE
    }

    private fun hideErrorView() {
        binding.errorContent.visibility = GONE
    }

    private fun showProgress() {
        binding.progressBar.visibility = VISIBLE
    }

    private fun handleError(error: Exception) {
        val errorContent = getErrorContent(error)
        binding.errorContent.setUp(errorContent)
    }

    private fun getErrorContent(error: Exception): ErrorContent = run {
        val retryAction = Pair(getString(R.string.retry_title), { viewModel.getProductById(args.productId) })
        when(error){
            is ResourceError.Network -> ErrorContent.Network(action = retryAction)
            is ResourceError.NotFound -> ErrorContent.NotFound()
            else -> ErrorContent.Unexpected(action = retryAction)
        }
    }

    private fun showProductDetail(product: Product) {
        showGeneralInformation(product)
        handleShippingInformation(product.shipping)
        showInventoryInformation(product)
        showSpecificInformation(product)
    }

    private fun showGeneralInformation(product: Product) {
        productDetailContentBinding.apply {
            title.text = product.name
            price.text = product.price.toLocalizedString()
            description.text = product.description
        }
        showProductImages(product.images)
    }

    private fun showProductImages(images: List<String>) {
        productDetailContentBinding.imageSlider.apply {
            setUp(images, { index: Int -> viewFullScreenImages(images, index) })
        }
    }

    private fun handleShippingInformation(shipping: Shipping) {
        productDetailContentBinding.freeShipping.visibility = if (shipping.free) VISIBLE else GONE
    }

    private fun showInventoryInformation(product: Product) {
        showSoldQuantity(product.soldQuantity)
        showAvailableQuantity(product.availableQuantity)
    }

    private fun showSoldQuantity(soldQuantity: Int) {
        productDetailContentBinding.soldQuantity.text = resources.getQuantityString(
            R.plurals.sold_quantity_label,
            soldQuantity,
            soldQuantity
        )
    }

    private fun showAvailableQuantity(availableQuantity: Int) {
        productDetailContentBinding.availableQuantity.text = resources.getQuantityString(
            R.plurals.available_quantity_label,
            availableQuantity,
            availableQuantity
        )
    }

    private fun showSpecificInformation(product: Product) {
        showSellerInformation(product.seller)
        showProductAttributes(product.attributes)
        product.warranty?.let { showWarranty(it) }
        showProductDescription(product.description)
    }

    private fun showSellerInformation(seller: Seller) {
        productDetailContentBinding.sellerInformationContent.apply {
            sellerLocation.text = getString(
                R.string.seller_location_label,
                seller.address.cityName,
                seller.address.stateName
            )
            this.root.visibility = VISIBLE
        }
    }

    private fun showProductAttributes(productAttribute: List<ProductAttribute>) {
        productDetailContentBinding.apply {
            productAttributes.adapter = ProductAttributesAdapter(productAttribute)
            productAttributesContent.visibility = VISIBLE
        }
    }

    private fun showWarranty(warranty: String) {
        productDetailContentBinding.apply {
            this.warranty.text = warranty
            warrantyContent.visibility = VISIBLE
        }
    }

    private fun showProductDescription(description: String) {
        productDetailContentBinding.apply {
            this.description.text = description
            descriptionContent.visibility = VISIBLE
        }
    }

    private fun viewFullScreenImages(images: List<String>, currentIndex: Int) {
        val action = ProductDetailFragmentDirections.actionViewFullScreenImages(
            images.toTypedArray(),
            currentIndex
        )
        findNavController().safeNavigate(action)
    }
}