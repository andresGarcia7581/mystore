package com.anfegara.mystore.presentation.views.prductsearch.productcatalog

import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem

interface CatalogItemListener {
    fun onCatalogItemSelected(catalogItem: CatalogItem)
}