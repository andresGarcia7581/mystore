package com.anfegara.mystore.presentation.views.prductsearch.productsuggestion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.anfegara.mystore.domain.catalog.domain.usecase.product.GetRecentProductQueriesUseCase
import com.anfegara.mystore.domain.catalog.domain.usecase.product.GetRecentProductQueriesByTextUseCase
import com.anfegara.mystore.domain.catalog.domain.usecase.product.SaveRecentProductQueryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val QUERY_ARGUMENT = "query"

@HiltViewModel
class ProductSuggestionViewModel @Inject constructor(
    getRecentProductQueriesUseCase: GetRecentProductQueriesUseCase,
    getRecentProductQueriesByTextUseCase: GetRecentProductQueriesByTextUseCase,
    private val saveRecentProductQueryUseCase: SaveRecentProductQueryUseCase,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _textQuery: MutableLiveData<String> =
        MutableLiveData<String>(savedStateHandle[QUERY_ARGUMENT] ?: String())
    val textQuery: LiveData<String> = _textQuery

    val filteredSuggestions: LiveData<List<String>> = textQuery.switchMap { query ->
        if (query.isBlank()) getRecentProductQueriesUseCase().asLiveData()
        else getRecentProductQueriesByTextUseCase(query).asLiveData()
    }.distinctUntilChanged()

    fun onTextQueryChanged(query: String) {
        val queryFormatted = query.trim()
        saveStateQuery(queryFormatted)
        _textQuery.value = queryFormatted
    }

    private fun saveStateQuery(query: String) {
        viewModelScope.launch { savedStateHandle[QUERY_ARGUMENT] = query }
    }

    fun handleQuerySubmit(query: String) {
        viewModelScope.launch(Dispatchers.Main) {
            saveRecentProductQueryUseCase(query)
        }
    }
}