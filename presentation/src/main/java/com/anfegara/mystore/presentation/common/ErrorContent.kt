package com.anfegara.mystore.presentation.common

import com.anfegara.mystore.presentation.R

sealed class ErrorContent constructor(
    open val idResourceImage: Int,
    open val title: TextResource,
    open val advice: TextResource,
    open val action: (Pair<String, () -> Unit>)? = null
) {
    data class Network(
        override val idResourceImage: Int = R.drawable.ic_network_error,
        override val title: TextResource = TextResource.fromStringId(R.string.network_error_message),
        override val advice: TextResource = TextResource.fromStringId(R.string.network_error_advice),
        override val action: (Pair<String, () -> Unit>)? = null
    ) : ErrorContent(idResourceImage, title, advice, action)

    data class NotFound(
        override val idResourceImage: Int = R.drawable.ic_generic_error,
        override val title: TextResource = TextResource.fromStringId(R.string.not_found_error_message),
        override val advice: TextResource = TextResource.fromStringId(R.string.not_found_error_advice),
        override val action: (Pair<String, () -> Unit>)? = null
    ) : ErrorContent(idResourceImage, title, advice, action)

    data class Unexpected(
        override val idResourceImage: Int = R.drawable.ic_generic_error,
        override val title: TextResource = TextResource.fromStringId(R.string.unexpected_error_message),
        override val advice: TextResource = TextResource.fromStringId(R.string.unexpected_error_advice),
        override val action: (Pair<String, () -> Unit>)? = null
    ) : ErrorContent(idResourceImage, title, advice, action)

    data class Custom(
        override val idResourceImage: Int,
        override val title: TextResource,
        override val advice: TextResource,
        override val action: (Pair<String, () -> Unit>)? = null
    ) : ErrorContent(idResourceImage, title, advice, action)
}