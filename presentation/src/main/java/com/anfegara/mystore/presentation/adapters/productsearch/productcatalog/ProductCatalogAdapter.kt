package com.anfegara.mystore.presentation.adapters.productsearch.productcatalog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.anfegara.mystore.domain.billing.domain.model.payment.Installments
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.databinding.CatalogItemViewBinding
import com.anfegara.mystore.presentation.views.prductsearch.productcatalog.CatalogItemListener

class ProductCatalogAdapter(private val listener: CatalogItemListener) :
    RecyclerView.Adapter<ProductCatalogAdapter.CatalogItemViewHolder>() {

    var productCatalog: List<CatalogItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogItemViewHolder =
        CatalogItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.catalog_item_view, parent, false),
            listener
        )

    override fun onBindViewHolder(holder: CatalogItemViewHolder, position: Int) {
        holder.bind(productCatalog[position])
    }

    override fun getItemCount(): Int = productCatalog.size

    class CatalogItemViewHolder(view: View, private val listener: CatalogItemListener) : RecyclerView.ViewHolder(view) {

        private val binding = CatalogItemViewBinding.bind(view)

        fun bind(catalogItem: CatalogItem) = binding.apply {
            thumbnail.load(catalogItem.thumbnail) { error(R.drawable.ic_error_loading_image) }
            title.text = catalogItem.title
            price.text = catalogItem.price.toLocalizedString()
            catalogItem.installments?.let { showInstallments(it) } ?: hideInstallments()
            setUpShipping(catalogItem.freeShipping)
            root.setOnClickListener { listener.onCatalogItemSelected(catalogItem) }
        }

        private fun showInstallments(installments: Installments) {
            binding.installments.apply {
                text = context.getString(
                    R.string.installments_label,
                    installments.quantity,
                    installments.amount.toLocalizedString()
                )
                visibility = View.VISIBLE
            }
        }

        private fun hideInstallments() {
            binding.installments.visibility = View.GONE
        }

        private fun setUpShipping(freeShipping: Boolean) =
            if (freeShipping) showShipping() else hideShipping()

        private fun showShipping() {
            binding.shipping.visibility = View.VISIBLE
        }

        private fun hideShipping() {
            binding.shipping.visibility = View.GONE
        }
    }
}