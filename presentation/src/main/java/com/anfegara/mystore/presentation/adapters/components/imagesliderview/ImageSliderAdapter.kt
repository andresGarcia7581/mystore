package com.anfegara.mystore.presentation.adapters.components.imagesliderview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.databinding.SlideableImageViewBinding

class ImageSliderAdapter(
    private var images: List<String> = emptyList(),
    private var clickListener: ((index: Int) -> Unit)? = null
) :
    RecyclerView.Adapter<ImageSliderAdapter.ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.slideable_image_view, parent, false)
        )

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(images[position], clickListener)
    }

    override fun getItemCount(): Int = images.size

    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = SlideableImageViewBinding.bind(view)

        fun bind(url: String, clickListener: ((Int) -> Unit)?) = binding.image.apply {
            load(url) { error(R.drawable.ic_error_loading_image) }
            setOnClickListener { clickListener?.invoke(adapterPosition) }
        }
    }

    fun setUp(images: List<String>, clickListener: ((index: Int) -> Unit)?) = apply {
        this.images = images
        this.clickListener = clickListener
        notifyDataSetChanged()
    }
}