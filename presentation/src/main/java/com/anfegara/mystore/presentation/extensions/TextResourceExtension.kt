package com.anfegara.mystore.presentation.extensions

import android.content.res.Resources
import com.anfegara.mystore.presentation.common.TextResource

fun TextResource.asString(resources: Resources): String = when (this) {
    is TextResource.SimpleTextResource -> this.text
    is TextResource.IdTextResource -> resources.getString(this.id)
}