package com.anfegara.mystore.presentation.views.prductsearch

import androidx.appcompat.app.AppCompatActivity
import com.anfegara.mystore.presentation.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductSearchActivity : AppCompatActivity(R.layout.activity_product_search)