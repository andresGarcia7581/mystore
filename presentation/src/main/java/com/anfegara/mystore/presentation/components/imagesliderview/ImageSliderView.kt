package com.anfegara.mystore.presentation.components.imagesliderview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager2.widget.ViewPager2
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.adapters.components.imagesliderview.ImageSliderAdapter
import com.anfegara.mystore.presentation.databinding.ImageSliderViewBinding

private const val MINIMUN_ITEMS_COUNTER_VISIBLE = 2

class ImageSliderView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr, defStyleRes) {

    private val adapter by lazy { ImageSliderAdapter() }
    private val binding = ImageSliderViewBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.slider.adapter = adapter
    }

    fun setUp(
        images: List<String>,
        clickListener: ((index: Int) -> Unit)? = null,
        currentIndex: Int = 0
    ) {
        adapter.setUp(images, clickListener)
        setUpImageChangeListener()
        setCurrentImageByIndex(currentIndex)
        refreshImageCounter(currentIndex)
    }

    private fun setCurrentImageByIndex(index: Int) {
        binding.slider.setCurrentItem(index, false)
    }

    private fun setUpImageChangeListener() {
        binding.slider.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                refreshImageCounter(position)
            }
        })
    }

    private fun refreshImageCounter(currentIndex: Int) {
        if (adapter.itemCount >= MINIMUN_ITEMS_COUNTER_VISIBLE) showImageCounter(currentIndex) else hideCounter()
    }

    private fun showImageCounter(currentIndex: Int) {
        binding.counter.apply {
            text = resources.getString(
                R.string.image_counter_label,
                currentIndex.inc(),
                adapter.itemCount
            )
            visibility = View.VISIBLE
        }
    }

    private fun hideCounter() {
        binding.counter.visibility = View.GONE
    }
}