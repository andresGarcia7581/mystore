package com.anfegara.mystore.presentation.views.prductsearch.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.anfegara.mystore.presentation.databinding.FragmentProductSearchBinding
import com.anfegara.mystore.presentation.extensions.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductSearchFragment : Fragment() {

    private lateinit var binding: FragmentProductSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSearchView()
    }

    private fun setUpSearchView() {
        binding.queryContent.root.setOnClickListener {
            val action = ProductSearchFragmentDirections.actionShowProductSuggestions(String())
            findNavController().safeNavigate(action)
        }
    }
}