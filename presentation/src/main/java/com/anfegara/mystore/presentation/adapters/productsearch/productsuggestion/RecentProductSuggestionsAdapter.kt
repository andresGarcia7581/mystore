package com.anfegara.mystore.presentation.adapters.productsearch.productsuggestion

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.databinding.ProductSuggestionViewBinding

class RecentProductSuggestionsAdapter(private val listener: (String) -> Unit) :
    ListAdapter<String, RecentProductSuggestionsAdapter.ProductSuggestionViewHolder>(
        RecentProductSuggestionsComparator()
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductSuggestionViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_suggestion_view, parent, false)
        return ProductSuggestionViewHolder(view, listener)
    }

    override fun onBindViewHolder(
        holder: ProductSuggestionViewHolder,
        position: Int
    ) {
        holder.bind(getItem(position))
    }

    class ProductSuggestionViewHolder(view: View, private val listener: (String) -> Unit) :
        RecyclerView.ViewHolder(view) {
        private val binding = ProductSuggestionViewBinding.bind(view)

        fun bind(recentSuggestion: String) {
            binding.suggestion.text = recentSuggestion
            binding.suggestion.setOnClickListener { listener.invoke(recentSuggestion) }
        }
    }

    class RecentProductSuggestionsComparator : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem.equals(newItem, ignoreCase = true)
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }
}