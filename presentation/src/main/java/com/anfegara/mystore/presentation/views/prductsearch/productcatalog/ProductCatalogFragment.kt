package com.anfegara.mystore.presentation.views.prductsearch.productcatalog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.anfegara.mystore.domain.catalog.readmodel.product.CatalogItem
import com.anfegara.mystore.domain.common.resource.Resource
import com.anfegara.mystore.domain.common.resource.ResourceError
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.adapters.productsearch.productcatalog.ProductCatalogAdapter
import com.anfegara.mystore.presentation.common.ErrorContent
import com.anfegara.mystore.presentation.common.TextResource
import com.anfegara.mystore.presentation.databinding.FragmentProductCatalogBinding
import com.anfegara.mystore.presentation.extensions.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductCatalogFragment : Fragment(), CatalogItemListener {

    private lateinit var binding: FragmentProductCatalogBinding
    private val productCatalogAdapter by lazy { ProductCatalogAdapter(this) }
    private val args: ProductCatalogFragmentArgs by navArgs()
    private val viewModel: ProductCatalogViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductCatalogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolBar()
        setUpCatalogUI()
        setUpData()
    }

    private fun setUpToolBar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
        setUpSearchField()
    }

    private fun setUpSearchField() {
        binding.queryContent.queryTextView.apply {
            text = args.query
            setOnClickListener { showANewQuerySuggestions() }
        }
    }

    private fun showANewQuerySuggestions() {
        val action =
            ProductCatalogFragmentDirections.actionSearchProductCatalogByNewQuery(args.query)
        findNavController().safeNavigate(action)
    }

    private fun setUpData() {
        viewModel.productCatalogState.observe(viewLifecycleOwner) {
            handleProductCatalogState(it)
        }
    }

    private fun setUpCatalogUI() {
        binding.products.apply {
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
            this.adapter = productCatalogAdapter
        }
    }

    private fun handleProductCatalogState(catalogState: Resource<List<CatalogItem>>) {
        resetInitialView()
        when (catalogState) {
            is Resource.Loading -> showProgress()
            is Resource.Success -> handleSuccessResult(catalogState.data)
            is Resource.Error -> handleError(catalogState.error)
        }
    }

    private fun resetInitialView() {
        hideProgress()
        hideErrorView()
    }

    private fun handleSuccessResult(productCatalog: List<CatalogItem>) {
        if (productCatalog.isEmpty()) showEmptyCatalogError() else showProductCatalog(productCatalog)
    }

    private fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }

    private fun hideErrorView() {
        binding.errorContent.visibility = View.GONE
    }

    private fun showEmptyCatalogError() {
        val error = ErrorContent.Custom(idResourceImage = R.drawable.ic_search,
            title = TextResource.fromStringId(R.string.empty_product_catalog_title),
            advice = TextResource.fromStringId(R.string.empty_product_catalog_advice))
        binding.errorContent.setUp(error)
    }

    private fun showProductCatalog(productCatalog: List<CatalogItem>) {
        productCatalogAdapter.productCatalog = productCatalog
    }

    private fun handleError(error: Exception) {
        val errorContent = getErrorContent(error)
        binding.errorContent.setUp(errorContent)
    }

    private fun getErrorContent(error: Exception): ErrorContent = run {
        val retryAction = Pair(getString(R.string.retry_title), { viewModel.getProductCatalogByQuery(args.query) })
        when(error){
            is ResourceError.Network -> ErrorContent.Network(title = TextResource.fromText(error.message), action = retryAction)
            else -> ErrorContent.Unexpected(action = retryAction)
        }
    }

    override fun onCatalogItemSelected(catalogItem: CatalogItem) {
        val action = ProductCatalogFragmentDirections.actionShowProductDetail(catalogItem.id)
        findNavController().safeNavigate(action)
    }
}