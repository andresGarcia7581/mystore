package com.anfegara.mystore.presentation.dialogs.fullscreenimageslider

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.databinding.DialogFullScreenImageSliderBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FullScreenImageSliderDialog : DialogFragment() {

    private lateinit var binding: DialogFullScreenImageSliderBinding
    private val args: FullScreenImageSliderDialogArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        super.onCreateDialog(savedInstanceState).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogFullScreenImageSliderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUI()
    }

    private fun setUpUI() = binding.apply {
        imageSlider.setUp(args.images.toList(), currentIndex = args.currentImageIndex)
        closeButton.setOnClickListener { dismiss() }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.apply {
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            setBackgroundDrawableResource(R.color.black_75_opacity)
        }
    }
}