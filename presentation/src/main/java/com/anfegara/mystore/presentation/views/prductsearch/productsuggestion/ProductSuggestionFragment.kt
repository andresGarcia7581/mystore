package com.anfegara.mystore.presentation.views.prductsearch.productsuggestion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.anfegara.mystore.presentation.R
import com.anfegara.mystore.presentation.adapters.productsearch.productsuggestion.RecentProductSuggestionsAdapter
import com.anfegara.mystore.presentation.databinding.FragmentProductSuggestionBinding
import com.anfegara.mystore.presentation.extensions.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductSuggestionFragment : Fragment(), SearchView.OnQueryTextListener {

    private lateinit var binding: FragmentProductSuggestionBinding
    private lateinit var searchView: SearchView
    private val suggestionsAdapter by lazy { RecentProductSuggestionsAdapter(this::handleSuggestionSelected) }
    private val viewModel: ProductSuggestionViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductSuggestionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolBar()
        setUpRecentSuggestions()
    }

    private fun setUpToolBar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.apply {
            setupWithNavController(navController, appBarConfiguration)
            inflateMenu(R.menu.search_menu)
        }.also { setUpSearchMenu(it.menu) }
    }

    private fun setUpRecentSuggestions() {
        binding.suggestions.adapter = suggestionsAdapter
        viewModel.filteredSuggestions.observe(viewLifecycleOwner) { recentSuggestions ->
            showRecentSuggestion(recentSuggestions)
        }
    }

    private fun showRecentSuggestion(recentSuggestions: List<String>) {
        suggestionsAdapter.submitList(recentSuggestions)
    }

    private fun setUpSearchMenu(menu: Menu) {
        searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.apply {
            isIconified = false
            queryHint = getString(R.string.search_hint)
            setQuery(viewModel.textQuery.value, false)
            setOnQueryTextListener(this@ProductSuggestionFragment)
            maxWidth = Integer.MAX_VALUE
        }.also { showKeyboard(it) }
    }

    private fun handleSuggestionSelected(suggestion: String) {
        searchView.setQuery(suggestion, false)
        handleNewQuery(suggestion)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        handleNewQuery(query)
        return true
    }

    override fun onQueryTextChange(query: String): Boolean {
        changeQuery(query)
        return true
    }

    private fun handleNewQuery(query: String) {
        if (query.isNotBlank()) {
            viewModel.handleQuerySubmit(query)
            goToProductCatalog(query)
        }
    }

    private fun goToProductCatalog(query: String) {
        val action = ProductSuggestionFragmentDirections.actionShowProductCatalogByQuery(query)
        findNavController().safeNavigate(action)
    }

    private fun changeQuery(query: String) {
        viewModel.onTextQueryChanged(query)
    }

    private fun showKeyboard(view: View) {
        ViewCompat.getWindowInsetsController(view)?.show(WindowInsetsCompat.Type.ime())
    }
}