# mystore

mystore will help you to search for the products you need, view a list with the different available options and view the detail 
of your favorite product.

## Preview

![App Preview](screenshots/app_preview.gif)

&nbsp;

## Architecture

Clean architecture allows us to represent the business domain, while promoting maintainability and low coupling through the separation 
of responsibilities through layers.

![Clean architecture](screenshots/clean_architecture_diagram.png)

### **Domain**

The domain layer is the core of the application, it encapsulates the business logic and the entities. 
It should not have any external dependency. Some DDD (Domain driven design) concepts were also used to organize the domain in a better way.

- **Bounded context (DDD Pattern)**

The Bounded context allows us to group different concepts and business objects that are related to each other.

- **Model (DDD Pattern)**
The model defines the business objects; each object must contain the logic that belongs to itself.

    - **Entity**
    An Entity is a business object defined by an unique id.
    
    - **Value Objects**
    Their identity is based on their properties and values (For example Address)
    
    - **Aggregate**
    An Aggregate is a collection of domain objects (entities and value objects, for example Product). It must have an unique id.

- **Use case**

An Use case is a set of communication steps for resolving a business operation, It may contain business logic.

- **Repository**

The Repository abstracts the operations that require access to data.

### **Data**

The Data layer encapsulates the data access logic, both local and remote. But it is not directly coupled to a framework.

### **Infraestructure**

The infrastructure layer contains external dependencies, such as database, http client, etc.

### **Presentation**

It is responsible to show the data to the user.

&nbsp;

## **Domain structure**

![Domain structure](screenshots/domain_package_diagram.jpg)

The Read Model is a model specialized for queries and data projections based in CQRS.

&nbsp;

## **quality assurance**

### **Testing**
Behaviour Driven Development(BDD).

### **continuous integration**

gitlab-ci.yml and local runner implemented to run tests, after every push.

&nbsp;

## **Good practices**

- Clean architecture.
- Android guidelines.
- DDD concepts.
- Clean code.
- SOLID.
- Dependency injection.
- Continuous integration.
- DRY.
- BDD.

&nbsp;

## **Tools and libraries**

* [Hilt](https://dagger.dev/hilt/) as dependency injector
* [Kotlin Coroutines](https://developer.android.com/kotlin/coroutines) for asynchronous tasks
* [View Model](https://developer.android.com/topic/libraries/architecture/viewmodel) for mvvm
* [Live Data](https://developer.android.com/topic/libraries/architecture/livedata?hl=es) for mvvm
* [Flow](https://developer.android.com/kotlin/flow?hl=es-419) for mvvm
* [Room](https://developer.android.com/jetpack/androidx/releases/room) as database
* [Navigation](https://developer.android.com/guide/navigation) for app navigation
* [Coil](https://coil-kt.github.io/coil/) as image loader
* [Retrofit](https://square.github.io/retrofit/) as http client
* [Moshi](https://github.com/square/moshi) for mapping kotlin classes 
* [mockito](https://site.mockito.org/) for testing
* [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics) for error logging

&nbsp;

### Andrés Felipe García Ramírez
andres.garcia7581@gmail.com
 
 



